#include "functions.h"

// Antes de tudo perceba que, da mesma forma que no local onde estas funções
// serão usadas, o arquivo `functions.h` é incluído aqui. Isto é para que as
// declarações das funções estejam disponíveis aqui também.

// Para implementar uma função declarada no header file (o arquivo `.h`),
// utilizamos a qualificação, semelhante ao que colocamos no momento em que
// chamamos a função, no nome. De resto, é tudo bastante semelhante ao
// que foi feito quando tínhamos um arquivo só.

std::vector<unsigned int> fibo::to_n(unsigned int n)
{
    auto fibs = std::vector<unsigned int>{};

    auto f1 = 1u;
    auto f2 = 1u;

    for (auto i = 0u; i < n; ++i) {
        fibs.push_back(f1);
        auto aux = f1;
        f1 = f2;
        f2 += aux;
    }

    return fibs;
}

// Então, resumindo, na implementação de uma função declarada no arquivo `.h`
// dentro de um namespace escrevemos o nome completo, totalmente qualificado,
// neste caso `fibo::to_n` em vez de apenas `to_n`. A função seguinte segue
// exatamente a mesma ideia.

std::vector<unsigned int> fibo::to_max(unsigned int max)
{
    auto fibs = std::vector<unsigned int>{};

    auto f1 = 1u;
    auto f2 = 1u;

    while (f1 <= max) {
        fibs.push_back(f1);
        auto aux = f1;
        f1 = f2;
        f2 += aux;
    }

    return fibs;
}

// Para a função a seguir, usaremos uma prática que pode até ser comum, mas que
// exige um pouco de cuidado.

// O que ocorre abaixo é que, para evitar sempre qualificar o nome da função,
// está se implementando ela dentro de uma *reabertura* do namespace `strings`.
// Qual a diferença entre esta prática e o que fizemos antes?

// A forma mais fácil de notar é a seguinte: em uma das funções anteriores,
// apenas aqui neste arquivo de implementação, adicione um argumento a mais,
// tente compilar e veja o erro que aparece.

// Na sequência, adicione um parâmetro a mais na próxima função. Faça a mesma
// experiência: o erro vai ser totalmente diferente: vai ser um erro do
// *linker*, e não do *compilador*.

// Agora faça mais uma experiência: faça uma segunda cópia da implementação de
// `fibo::to_n`, mas na cópia adicione mais um parâmetro. Tente compilar e note
// bem o erro.

// Por fim, faça a mesma experiência com `palindrome` logo abaixo, colocando a
// cópia também dentro do namespace `strings`. Não esqueça de adicionar um
// parâmetro extra na cópia. Tente compilar... e note que nada de errado
// ocorre.

// O que acontece é que no tipo de definição que temos acima, o compilador tem
// certeza absoluta de que uma declaração já deveria ter existido
// anteriormente. Assim, se ocorrer alguma discrepância entre a declaração e a
// definição, o compilador pode avisar de forma (razoavelmente) amigável.

// No tipo de definição que temos abaixo, com o namespace reaberto, o
// compilador não tem como verificar que não é o objetivo do programador
// declarar e definir uma função nova. Portanto, no primeiro experimento o erro
// acaba caindo no processo de linkagem, que explicaremos ao final desta aula.
// No segundo experimento, nenhum erro ocorre, pois do ponto de vista do
// compilador, não existe nada de errado.

// Portanto, apenas utilize a forma seguinte se a legibilidade do código acabar
// muito melhor, e acima de tudo, os programadores que mexem no código sejam
// muito bem comportados. Dificilmente acontecerão bugs por causa disso, mas
// código inútil pode ser deixado para trás, por exemplo, ou erros confusos de
// linkagem podem ocorrer em vez de erros de mais fácil interpretação como os
// do compilador.

namespace strings {

bool is_palindrome(const std::string &s)
{
    auto i = s.size() - 1;

    for (auto j = 0u; j < s.size()/2; ++j) {
        if (s[j] != s[i]) {
            return false;
        }
        --i;
    }

    return true;
}

}

// No caso a seguir, continuamos com o namespace fechado, assegurando que está
// sendo implementada a função previamente declarada no namespace `vectors`
// através do header "functions.h", e não criando uma nova função (diferente
// das declaradas) por engano.

void vectors::zero_all(std::vector<unsigned int> &ints)
{
    for (auto &i: ints) {
        i = 0;
    }
}

