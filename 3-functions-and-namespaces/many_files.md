Gerando um programa a partir de diversos arquivos
=================================================

Na aula de hoje vimos um exemplo de um programa dividido em mais de um arquivo
`.cpp`. No próprio código, vimos que é possível compilar a aula com o comando

```
g++ -o many_files *.cpp -std=c++14
```

Porém, para programas maiores, com diversos arquivos, isto não é realmente muito
manutenível. Programas maiores costumam ser compilados de uma forma um pouco
diferente.


Unidades de Tradução
--------------------

O compilador encara o código em C++ como **unidades de tradução**. Uma unidade de
tradução, conforme veremos abaixo, é algo que gera um código objeto. Do nosso
ponto de vista, é um arquivo `.cpp`.

Um ponto interessante é que do ponto de vista do *compilador* os headers (`.h`)
não existem. O compilador enxerga o conteúdo deles como parte dos `.cpp` em que
eles são incluídos, apenas.


Os estágios de compilação
-------------------------

Um programa em C++ passa por pelo menos 5 estágios durante sua compilação.

### Preprocessamento

O primeiro estágio é o **preprocessamento**. Esse estágio na realidade vem antes
da compilação propriamente dita: o preprocessador é um programa de manipulação
de texto que não leva em consideração que o código é C ou C++ ou o que seja.
Ele apenas manipula texto de acordo com diretivas, as que vemos no código
iniciando com `#`, como `#include` ou `#ifndef`.

Para observar o resultado do preprocessamento, pode-se utilizar o comando `cpp`
(C Preprocessor, nada a ver com C++ diretamente), ou `g++ -E`. Estes comandos
jogarão o resultado para a saída padrão. Para gerar um arquivo com a saída,
usamos a opção `-o`. A extensão mais comum para arquivos preprocessados é
`.i`.

```
cpp main.cpp -o main.i -std=c++14
```

```
g++ -E main.cpp -o main.i -std=c++14
```

### Compilação

Na sequência ocorre o passo que chamamos **compilação** propriamente dita: o
código em C++ é compilado para  _assembly_, ou **linguagem de montagem**.

Podemos observar este estágio utilizando a opção `-S` com o `g++`. Novamente
a saída é dada na saída padrão. Para gerar um arquivo, utilizamos a opção `-o`.

```
g++ -S main.cpp -o main.S -std=c++14
```

ou, partindo de onde paramos antes

```
g++ -S main.i -o main.S -std=c++14
```

### Montagem

Neste passo o _assembly_ é **montado**, gerando código de máquina binário. Isto
gera um código que chamamos _object code_ ou **código objeto**. Para
visualizarmos este passo temos a opção `-c`.

```
g++ -c main.cpp -o main.o -std=c++14
```

ou partindo do ponto anterior

```
g++ -c main.cpp -o main.o -std=c++14
```

Perceba que se abrirmos o código objeto num editor de texto comum vemos diversos
caracteres estranhos: o formato agora é binário, e não textual. Podemos ver o
conteúdo do arquivo de forma mais legível com o comando `objdump -CD` (sugestão:
jogue a saída para o comando `less` para poder navegar, pois a saída é grande).

```
objdump -CD main.o | less
```

A flag `-C` é especial para C++ e faz com que nomes sejam mostrados como no
código, com namespaces corretos e afins.

### Linkagem

O processo final de geração do executável é a **linkagem** (que admitidamente
não é um verbo real em português). Neste passo, os códigos objeto de todas as
unidades de tradução envolvidas no programa são unidos em um só.

Para ver isso com o nosso código de exemplo, repita o passo anterior com o
arquivo `functions.cpp` e rode

```
g++ -o many_files main.o functions.o
```

Será gerado um executável de nome `many_files`, que é o mesmo programa que
geramos com o comando `g++ -o many_files *.cpp -std=c++14`. Obviamente,
neste exemplo bastante simples, isso tudo parece uma complicação desnecessária.
Realmente, exceto o comando `g++ -c` para gerar código objeto, os outros não
costumam ser utilizados com frequência. Em programas maiores, porém, com
processos de compilação mais complexos, com diversos arquivos, e principalmente,
em que nem todos os .cpp estão no mesmo diretório, a geração de código objeto
intermediária é importante e bastante comum.

Diagrama
--------

Por fim, para uma visualização mais "gráfica", temos aqui um diagrama.

![Diagrama do processo de geração de um executável](images/compiler_diagram.png)
