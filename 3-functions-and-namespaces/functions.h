// Código em vários arquivos - o *header*
// --------------------------------------

// Em C++, para podermos começar a dividir as unidades do nosso programa em
// vários arquivos, necessitamos de _headers_. Este arquivo aqui é um _header_,
// e para diferenciá-lo, utilizamos a extensão `.h`.

// Headers são utilizados para compartilhar definições importantes de forma que
// diversas partes do programa tenham acesso. Para isso, é utilizado um
// mecanismo chamado _include_. Includes nada mais são que, literalmente, a
// inclusão do texto de um arquivo em um determinado ponto em outro arquivo.
// Portanto, quando damos um `#include <headername>` estamos literalmente
// inserindo todo o texto de um cabeçalho naquele ponto do programa. Esse
// mecanismo de inclusão é consideravelmente primitivo e vem de um legado
// histórico consideravelmente antiquado, mas propostas de sistemas melhores
// existem, com previsão para C++20. Independentemente destas novas propostas,
// muito código usará o sistema de includes por muito tempo, então é importante
// entendê-lo.

// Como headers podem incluir outros headers (faremos isso em poucas linhas), e
// quem inclui um header não tem como saber quais outros headers foram
// incluidos por ele, existe a possibilidade de um header acabar sendo incluído
// duas vezes. Para evitar que isso aconteça, utilizamos _include guards_, que
// são as linhas na sequência.

#ifndef EXAMPLE_FUNCTIONS_H_
#define EXAMPLE_FUNCTIONS_H_

// As linhas acima fazem o seguinte: se o símbolo `EXAMPLE_FUNCTIONS_H_` não
// estiver definido, ou seja, se este header nunca foi incluido antes, todo o
// texto entre a linha `#ifndef` e a linha `#endif` será considerado, e o
// símbolo será definido. Se não, tudo entre `#ifndef` e `#endif` será pulado.

// Desta forma, os include guards impedem que o texto de um header seja
// incluído duas vezes. Com isso, evita-se que o compilador precise ler o mesmo
// código várias vezes para compilar um `.cpp` só, e em alguns casos evitam-se
// erros de múltiplas definições.

#include <vector>
#include <string>

// Desta vez, como estamos separando nosso programa em vários arquivos, iremos
// dividir as *declarações* das *definições*.

// O que são declarações e definições em C++?

// Declarações introduzem para o compilador algum item: uma função, uma classe,
// uma variável.

// Definições por sua vez são completas: no caso de funções, possuem todo o
// corpo, ou seja, a implementação completa da função.

// Toda definição também é uma declaração, mas nem toda declaração é uma
// definição.  É importante prestar atenção nisso. Este arquivo aqui contém
// apenas declarações.

namespace fibo {

// As funções neste namespace são bastante simples: o parâmetro é um
// inteiro sem sinal, e o retorno é uma sequência de valores, aqui
// representada por um `vector`. Para uma descrição do que estas funções
// fazem especificamente, veja os exercícios da aula 3.

// Declarações de funções são formadas pelo tipo de retorno e a assinatura
// da função, seguidas de um `;`. Note a falta do corpo da função entre
// chaves. Essa parte virá no arquivo seguinte, o `.cpp` com a definição das
// funções.

std::vector<unsigned int> to_n(unsigned int n);
std::vector<unsigned int> to_max(unsigned int max);

}

namespace strings {

// Nesta função temos uma novidade: o parâmetro que é passado possui, após o
// tipo, um `&`, significando "referência". Para entender isto melhor, veja o
// apêndice 2 desta aula. Em resumo, uma referência permitirá que tenhamos
// acesso não à uma cópia, mas à string original passada como parâmetro. Como o
// parâmetro é `const`, não podemos alterá-la, apenas ler. Assim, evitamos uma
// cópia, potencialmente custosa no caso de uma string grande, do seu conteúdo.

bool is_palindrome(const std::string &s);

}

namespace vectors {

// Aqui vemos um exemplo de referência (novamente, apêndice 2), mas sem
// `const`, ou seja, potencialmente haverá modificação no vector original
// passado por parâmetro.

void zero_all(std::vector<unsigned int> &ints);

}

// As declarações presentes neste arquivo são suficientes tanto para definir as
// funções em um `.cpp` quanto para que outro código consiga chamá-las. A união
// entre os códigos `.cpp` diferentes será feita posteriormente pelo compilador,
// como veremos.

#endif
