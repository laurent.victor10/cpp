C++
===

Este repositório contém material de apoio para o ensino de C++.

Aulas
-----

* [Aula 1 - Introdução e sintaxe básica](1-introduction-and-basic-syntax)
* [Aula 2 - O tipo padrão std::vector](2-the-standard-vector-type)
* [Aula 3 - Funções e Namespaces](3-functions-and-namespaces)

Material de apoio
-----------------

Referências da linguagem e da biblioteca padrão podem ser encontradas
em:

* http://en.cppreference.com/w/
* http://www.cplusplus.com/

Compiladores online:

* https://ideone.com/ (C++14, apenas um .cpp)
* http://www.tutorialspoint.com/compile_cpp0x_online.php
  (C++14, e permite dividir o código em arquivos)

Licenças
--------

Copyright (c) 2016 João Paulo Taylor Ienczak Zanette,
                   Tarcísio Eduardo Moreira Crocomo

* O material didático se encontra sob licença
  [CC BY-SA](https://creativecommons.org/licenses/)
* O código se encontra sob a licença [MIT](LICENSE_CODE.md)
