#include <iostream>  // std::cout
#include <vector>


int main()
{
    // Acessando elementos
    // -------------------

    // Os elementos de um vetor podem ser acessados pelo operador `[]`

    auto v = std::vector<int>{1, 2, 3, 4};
    auto w = std::vector<int>{4, 3, 2, 1};

    std::cout << "v[0] = " << v[0] << ", w[2] = " << w[2] << "\n";

    // Perceba que o primeiro elemento é o de índice 0. Isso não deve ser uma
    // novidade muito grande para quem já conhece outras linguagens de
    // programação, mas pode causar um pouco de confusão de início. Por causa
    // disso, se um vetor tiver n elementos, o último tem o índice n-1.
    // Ou seja, o último elemento de `w` é `w[3]`, pois `w` possui tamanho 4.

    // Vetores são "objetos". Em C++, "objetos" são todos os valores que não são
    // de tipos primitivos. Vetores não são os primeiros objetos vistos por nós:
    // `std::string`s também são objetos.

    // Uma característica principal de objetos é agruparem diversas
    // informações. Um vetor, por exemplo, é capaz de informar o seu tamanho,
    // ou seja, de quantos objetos ele é composto no momento, com a
    // função-membro `size`.

    std::cout << "v.size() = " << v.size() << "\n";

    // Funções-membro, também chamadas de "métodos" são rotinas de código
    // atreladas ao objeto. Veremos mais adiante o conceito de função,
    // função-membro e como criar objetos. Por agora, apenas mantenha esses
    // nomes em um canto da mente :)

    // Outros métodos interessantes de acesso em `vector` são os seguintes:

    // Obtém o primeiro elemento do vetor (o elemento "da frente")
    std::cout << "v.front() = " << v.front() << "\n";

    // Obtém o último elemento do vetor (o elemento "de trás")
    std::cout << "v.back() = " << v.back() << "\n";

    // Outras informações podem ser adquiridas com `std::vector`, por exemplo,
    // verificar se ele está vazio (ou seja, com 0 elementos) com `empty`:

    std::vector<int> q = {};

    std::cout << "q.empty() = " << q.empty() << "\n";

    // Note que `empty` retorna um `bool`, e pode ser utilizado em estruturas
    // condicionais (`if`):

    if (q.empty()) {
        std::cout << "q is empty\n";
    } else {
        std::cout << "there's something inside q\n";
    }

    // Tente inicializar `q` com algum elemento para mudar o comportamento de
    // `empty`.

    return 0;
}
