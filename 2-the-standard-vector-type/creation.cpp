#include <vector>  // std::vector


int main()
{
    // O que é um `std::vector` e como criá-lo?
    // ----------------------------------------

    // A primeira questão é: como criar um objeto do tipo `vector`?
    // `vector` difere um tanto dos tipos primitivos e do tipo `string`
    // por ser *parametrizado*, e portanto sua declaração vai ser um pouco
    // diferente.

    // Um tipo parametrizado, também conhecido como um "template de classe"
    // ("_class template_") não é realmente um tipo, e sim uma "fábrica de
    // tipos". Ele recebe parâmetros através da sintaxe  de parênteses
    // angulados (`<>`). O tipo `vector` recebe até dois parâmetros, mas para a
    // aula de hoje vamos nos preocupar apenas com o primeiro: o tipo dos
    // elementos do vetor.

    auto v = std::vector<int>{}; // v é um vetor (ou "array dinâmico", ou
                                 // "array list") de `int`s.

    // Um vetor é o que chamamos de "coleção". Ele serve como um recipiente
    // para vários elementos do mesmo tipo. O tipo de coleção que um vetor
    // representa é o que chamamos de "sequência", pois a ordem dos elementos
    // é mantida e importa.

    // Podemos colocar diversos elementos em v com o que chamamos de "lista
    // de inicialização", ou "initializer list". Isso é uma funcionalidade
    // de C++ moderno (C++11 e adiante), então não se surpreenda em ver código
    // complexo para fazer esta mesma coisa, especialmente em código antigo.

    // Veja no Apêndice 1 como se fazia isso antes de C++11.

    v = {1, 2, 3, 4};

    // Isso também pode ser feito diretamente ao declarar o vetor.

    auto w = std::vector<int>{4, 3, 2, 1};

    // Sinta-se à vontade (melhor, sinta-se intimadx!) para alterar a linha
    // acima. Insira elementos, remova elementos, altere elementos. Sempre
    // recompile e veja o resultado. Tente colocar elementos que não sejam
    // do tipo `int`, como `double`s ou mesmo `std::string`s.
}
