#include <vector>
#include <iostream>


int main()
{
    // Alterando elementos
    // -------------------

    auto v = std::vector<int>{1, 2, 3, 4};
    auto w = std::vector<int>{4, 3, 2, 1};

    // Da mesma forma que é possível acessar um elemento no vetor com o
    // operador `[]`, é possível substituir o elemento naquele ponto.

    std::cout << "Before: v[3] = " << v[3] << "\n";

    v[3] = 42;

    std::cout << "Now: v[3] = " << v[3] << "\n";

    // Também é possível adicionar novos elementos ao final do vetor, utilizando
    // `push_back(...)`:

    auto n = v.size();

    std::cout << "n = v.size() = " << n << "\n";

    std::cout << "Before: v[n-1] = " << v[n-1] << "\n";

    v.push_back(777);

    n = v.size();  // Atualiza tamanho do vetor

    std::cout << "n = v.size() = " << n << "\n";
    std::cout << "Now: v[n-1] = " << v[n-1] << "\n";

    std::cout << "v[n-2] = " << v[n-2] << "\n";

    // Da mesma for, é possível remover o último elemento do vetor, através do
    // `pop_back`:

    v.pop_back();

    n = v.size();

    std::cout << "Popped vector's back\n";
    std::cout << "n = v.size() = " << n << "\n";
    std::cout << "Now: v[n-1] = " << v[n-1] << "\n";

    std::cout << "v[n-2] = " << v[n-2] << "\n";

    // Por fim, por `std::vector` ser um tipo parametrizado, é possível criar
    // vectors de diferentes tipos, como por exemplo, `std::string`:

    auto strings = std::vector<std::string>{ "Hello", "World", "!" };

    std::cout << "strings = {\""
              << strings[0] << "\", \""
              << strings[1] << "\", \""
              << strings[2] << "\"}\n";

    std::cout << "With strings' contents we can say... you had it coming:\n";
    std::cout << strings[0] << ", " << strings[1] << strings[2] << "\n";

    // E isso mantém todas as características de um vector, incluindo `size`,
    // `push_back`, `front`, e demais comandos demonstrados durante o código.

    // Note que não é possível inserir um `int` em `strings`. Descomente a
    // linha abaixo para ver isso, pois ela não compila.  strings.push_back(3);

    // A tipagem parametrizável de `vector` ocorre durante a compilação. Desta
    // forma, `std::vector<int>` e `std::vector<std::string>` são tipos
    // completamente diferentes para o compilador, e erros como o acima são
    // facilmente evitados.

    // **Nota**:
    // Há também operações como remover elementos do meio de um vetor, porém
    // estas lidam com conceitos que serão vistos mais adiante (especificamente,
    // iterators) e portanto só serão vistas quando tais conceitos estiverem
    // devidamente explicados

    return 0;
}


