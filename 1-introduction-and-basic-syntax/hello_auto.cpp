#include <iostream>


int main()
{
    // Revisitando nosso programa simples
    // ----------------------------------

    // Refaremos aqui, rapidamente nosso programa simples, com apenas
    // três pequenas diferenças.

    auto year = 2016;
    auto age = 23u;

    auto name = std::string{"Tarcísio"};

    std::cout << "Hello, " << name
              << ". I see you were born in "
              << year - age << "!\n";

    // Aqui começamos a utilizar uma pequena "novidade" existente a partir do
    // C++11, a keyword `auto`, que permite declarar uma variável cujo tipo
    // será o tipo do valor utilizado em sua inicialização.

    // Utilizar `auto` frequentemente nos traz duas vantagens:
    // 1. Não temos como declarar variáveis sem inicializá-las, pois
    //    ao declarar com `auto` é necessário inicializar para que
    //    o tipo possa ser deduzido.
    //
    // 2. Não há repetição de informação no que escrevemos. Ao
    //    escrevermos `int year = 2016;`, a informação de que a variável é um
    //    int existe tanto no nome `int` como no valor `2016`, que já é um
    //    `int` para a linguagem. Logo,

    // Utilizaremos sempre que possível essa forma de declarar variáveis. Assim,
    // manteremos consistência, inclusive com outras construções da linguagem
    // que veremos adiante. Se quiser mais informação sobre esse estilo de
    // escrever C++, olhe a referência [1] desta aula (atenção: não é uma
    // leitura introdutória).

    return 0;
}
