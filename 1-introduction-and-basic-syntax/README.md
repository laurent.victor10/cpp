Aula 1 - Introdução e sintaxe básica
====================================

Bem vindos à primeira aula de C++! Nela abordaremos a sintaxe básica
da linguagem, as estruturas de controle `if`, `while` e `for` e alguns
tipos básicos para podermos fazer programas básicos. Veremos tipos
primitivos da linguagem e também um primeiro tipo composto da biblioteca
padrão, o tipo `std::string`.

Seções
------

Esta aula está dividida em seções, cada uma demonstrada em um trecho de
código.

1. [Um programa simples em C++](hello.cpp)
2. [Revisitando nosso programa simples](hello_auto.cpp)
3. [A construção `if/else if/else`](if.cpp)
4. [O laço `while`](while.cpp)
5. [O laço `for`](for.cpp)


Exercícios
----------

**Nota**: Como ainda não vimos funções, os exercícios podem ser feitos direto
          dentro de `main`, com as variáveis dadas no enunciado definidas
          de forma "hardcoded".

1. Escreva um laço que passe **por todos** os números de 1 a 100,
   mas imprima apenas os pares.
    * Dica: o operador `%` pode ser utilizado para obter o resto da divisão
      inteira.
2. Faça um laço baseado no da questão anterior. Neste laço, para cada número,
   imprima:
    * `fizz` se o número for divisível por 3
    * `buzz` se o número for divisível por 5
    * `fizzbuzz` se o número for divisível por ambos
    * O próprio número, em todos os outros casos.
    * Lembre de **não** imprimir o número nos três primeiros casos.
3. Utilizando algum dos tipos de laço vistos em aula, imprima os valores
   da sequência de Fibonacci, das seguintes formas:
    * Dado um `n`, até o `n`-ésimo elemento da sequência.
    * Dado um `n`, até o último elemento da sequência que seja menor ou igual
      a `n`
    * **Dica**: Considere que o `n` do primeiro elemento é 0, e que os
      dois primeiros elementos (`n` 0 e 1) tem valor 1.
4. Utilizando algum dos tipos de laço vistos em aula, dada uma std::string
   qualquer, imprima na tela `palindrome` se ela for um palíndromo e
   `not a palindrome` se ela não for um palíndromo.
    * Para acessar cada caractere da string, pode se usar `s[i]` para uma
      `string s` e uma posição `i`, ou pode-se usar o _range-based_ `for` na
      string.
    * Lembre-se que as posições na `string` iniciam-se em 0.


Referências
-----------

1. [GotW #94 Solution: AAA Style (Almost Always Auto) | Sutter’s Mill](http://herbsutter.com/2013/08/12/gotw-94-solution-aaa-style-almost-always-auto/)
2. [Newton's Square Root Approximation by Ron Kurtus](http://www.school-for-champions.com/algebra/square_root_approx.htm)
