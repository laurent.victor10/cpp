#include <iostream>


int main()
{
    // A construção `if/else if/else`
    // ------------------------------

    // Aqui temos um código baseado em uma condição. Temos abaixo uma nota,
    // representada como um `double`. Baseado no valor dessa nota, o código
    // imprime uma mensagem relacionada.

    auto grade = 5.0;

    if (grade < 5.75) {
        std::cout << "Sorry, not this time.\n";
    } else if (grade < 8.0) {
        std::cout << "It's ok.\n";
    } else {
        std::cout << "Congratulations on that!\n";
    }

    // Um bloco `if/else if/else` funciona da seguinte maneira: a condição dada
    // dentro dos parênteses na construção `if` é uma condição booleana, ou
    // seja, pode ser verdadeira ou falsa (em C++, tipo `bool` e valores `true`
    // ou `false`). No caso, temos `grade < 5.75`, o que pode ser verdadeiro ou
    // falso dependendo do valor de `grade` (altere o valor para observar
    // os diferentes comportamentos).

    // O código contido entre as chaves do bloco if só irá executar se a
    // condição for verdadeira. Se ela não for verdadeira, o código é pulado.

    // Já nas cláusulas `else`, a execução do código não se baseia apenas na
    // condição. É necessário que as cláusulas anteriores NÃO tenham executado
    // para que o código contido no bloco de um `else` seja executado. Portanto,
    // os blocos de uma construção condicional `if/else if/else` são *mutuamente
    // exclusivos*.

    // Por causa do que foi explicado acima, quando `grade` é `5.0`, o segundo
    // bloco não executa, mesmo que a condição seja verdadeira. Como o primeiro
    // bloco executa e o segundo bloco só irá executar em uma condição de "se
    // não", a condição não é suficiente para que o código execute.

    return 0;
}
